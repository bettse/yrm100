meta:
  id: yrm100
  endian: be
seq:
  - id: header
    type: u1
  - id: type
    type: u1
    enum: msgtype
  - id: payload
    type:
      switch-on: type
      cases:
        'msgtype::command': command
        'msgtype::response': response
        'msgtype::notification': notification
  - id: checksum
    type: u1
  - id: end
    type: u1
types:
  command:
    seq:
      - id: command
        type: u1
      - id: len_parameter
        type: u2
      - id: parameter
        size: len_parameter
  response:
    seq:
      - id: command
        type: u1
      - id: pl
        type: u2
      - id: info_type
        type: u1
      - id: info
        size: pl - 1
  notification:
    seq:
      - id: command
        type: u1
      - id: len_parameter
        type: u2
      - id: rssi
        type: u1
      - id: pc
        type: u2
      - id: epc
        size: len_parameter - 5
      - id: crc
        type: u2
enums:
  msgtype:
    0: command
    1: response
    2: notification

