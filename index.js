const { SerialPort } = require("serialport");
const { ReadlineParser } = require("@serialport/parser-readline");
const { DelimiterParser } = require("@serialport/parser-delimiter");
const {
  InterByteTimeoutParser,
} = require("@serialport/parser-inter-byte-timeout");
const inquirer = require("inquirer");
const epcTds = require("epc-tds");

const KaitaiStream = require("kaitai-struct/KaitaiStream");
const Yrm100 = require("./Yrm100");
const { Msgtype } = Yrm100;

const HEX = 0x10;
const { DEBUG, SERIAL_PORT = "/dev/cu.SLAB_USBtoUART" } = process.env;

const COMMANDS = {
  INFORMATION: 0x03,
  SET_SELECT_PARAMS: 0x0C,
  SINGLE_POLL: 0x22,
  START_MULTI_POLL: 0x27,
  STOP_MULTI_POLL: 0x28,
  WRITE_LABEL_DATA_STORE: 0x49,
};

const SEL_PARAMS = {
  RFU: 0x00,
  EPC: 0x01,
  TID: 0x02,
  USER: 0x03,
};

const HardwareVersion = Buffer.from("bb0003000100047e", "hex");
const SoftwareVersion = Buffer.from("bb0003000101057e", "hex");
const Manufacturer = Buffer.from("bb0003000102067e", "hex");
const SinglePoll = Buffer.from("bb00220000227e", "hex");
const StartMultiPoll = Buffer.from("bb00270003222710837e", "hex");
const StopMultiPoll = Buffer.from("bb00280000287e", "hex");
const ReadLabelTID = Buffer.from("bb00390009 000000000200000002FF7e", "hex");
const ReadLabelUser = Buffer.from("bb00390009000000000300000002FF7e", "hex");
const SetSelectModeEPC = Buffer.from("bb0012000101147e", "hex");
const SetSelectModeTID = Buffer.from("bb0012000102157e", "hex");
const SetSelectModeUSER = Buffer.from("bb0012000103167e", "hex");
const GetSelectParams = Buffer.from("bb000b00000b7e", "hex");

let runningMultiplePolling = false;

// TODO: use inquire to select serial port if none provided
if (!SERIAL_PORT) {
  console.log("SERIAL_PORT not defined");
  process.exit(1);
}

const port = new SerialPort({ path: SERIAL_PORT, baudRate: 115200 });
const parser = port.pipe(
  new DelimiterParser({ delimiter: [0x7e], includeDelimiter: true })
);
const ui = new inquirer.ui.BottomBar();

let lastTag = null;

port.on("error", (err) => {
  console.error(err);
});

port.on("open", () => {
  console.log("ready");
  commandPrompt();
});

port.on("close", () => {
  console.log("closed");
  process.exit(0);
});

parser.on("data", (data) => {
  try {
    if (!verifyChecksum(data)) {
      console.log("ignoring bad checksum");
      return;
    }

    const { type, payload } = new Yrm100(new KaitaiStream(data));
    switch (type) {
      case Msgtype.RESPONSE:
        const { command, infoType } = payload;
        if (command === 0xff) { // Error
          if (infoType == 0x15) {
            // When multiple polling, just watch for the first success
            if (!runningMultiplePolling) {
              ui.updateBottomBar("no tag");
            }
          } else {
            const { info } = payload;
            console.log('Error', {
              command: command.toString(HEX),
              infoType: infoType.toString(HEX),
              info: Buffer.from(info).toString("hex"),
            });
          }
          break;
        } else {
          switch (command) {
            case COMMANDS.STOP_MULTI_POLL:
              runningMultiplePolling = false;
              break;
            case COMMANDS.INFORMATION:
              ui.updateBottomBar(Buffer.from(payload.info).toString("ascii"));
              break;
            case COMMANDS.SET_SELECT_PARAMS:
              console.log({
                command: command.toString(HEX),
                infoType: infoType.toString(HEX),
              });

              console.log("Send write EPC");
              port.write(
                writeEPC(
                  lastTag.pc,
                  lastTag.crc,
                  Buffer.from("000000000000002004C82A7A", "hex")
                )
              );
              break;
            case COMMANDS.WRITE_LABEL_DATA_STORE:
              console.log("Write EPC success");
              break;
            default:
              console.log('unhandled', {
                command: command.toString(HEX),
                infoType: infoType.toString(HEX),
              });
              break;
          }
        }
        break;
      case Msgtype.NOTIFICATION:
        const { crc, pc, epc: rawEpc } = payload;
        const epc = Buffer.from(rawEpc);
        lastTag = {
          crc: Buffer.from(crc.toString(HEX), 'hex'),
          pc: Buffer.from(pc.toString(HEX), 'hex'),
          epc
        };
        let epcTag = epcTds.valueOf(
          `${pc.toString(HEX)}${epc.toString("hex")}`
        );
        ui.updateBottomBar(
          `CRC: ${crc.toString(HEX)} PC: ${pc.toString(HEX)} EPC: ${epc.toString(
            "hex"
          )} -- ${epcTag.toIdURI()}`
        );
        if (runningMultiplePolling) {
          port.write(StopMultiPoll);
        } else {
          console.log("Set Select Params");
          port.write(setSelectParams(epc));
        }

        break;
    }
  } catch (e) {
    console.log(e);
  }
});

let lastAction = 0;
const actionList = [
  { name: "Single Poll", value: "single_poll", short: "Single Poll" },
  { name: "Read TID", value: "read_tid", short: "Read TID" },
  { name: "Read USER", value: "read_user", short: "Read USER" },
  /*
  { name: "Set Mode: EPC", value: "set_mode_epc", short: "EPC" },
  { name: "Set Mode: TID", value: "set_mode_tid", short: "TID" },
  { name: "Set Mode: USER", value: "set_mode_user", short: "USER" },
  { name: "Get Selected Parameter", value: "get_selected_param", short: "Get Selected" },
  */
  { name: "Hardware Version", value: "hardware_version", short: "Hardware" },
  { name: "Software Version", value: "software_version", short: "Software" },
  {
    name: "Poll until tag",
    value: "start_multi_poll",
    short: "Multi Poll",
  },
  { name: "Close", value: "close", short: "Close" },
  { name: "", value: "", short: "" },
];

const stopMultiplePolling = [
  { name: "Stop Multiple Poll", value: "stop_multi_poll", short: "Stop" },
];

function commandPrompt() {
  inquirer
    .prompt([
      {
        name: "action",
        message: "What do you want to do?",
        type: "list",
        choices: runningMultiplePolling ? stopMultiplePolling : actionList,
        default: lastAction,
      },
    ])
    .then((answers) => {
      const { action } = answers;
      lastAction = action;
      switch (action) {
        case "":
          break;
        case "close":
          port.close();
          break;
        case "single_poll":
          port.write(fixChecksum(SinglePoll));
          break;
        case "hardware_version":
          port.write(HardwareVersion);
          break;
        case "software_version":
          port.write(SoftwareVersion);
          break;
        case "start_multi_poll":
          runningMultiplePolling = true;
          port.write(StartMultiPoll);
          break;
        case "stop_multi_poll":
          runningMultiplePolling = false;
          port.write(StopMultiPoll);
          break;
        case "read_user":
          port.write(fixChecksum(ReadLabelUser));
          break;
        case "read_tid":
          port.write(fixChecksum(ReadLabelTID));
          break;
        case "set_mode_epc":
          port.write(SetSelectModeEPC);
          break;
        case "set_mode_tid":
          port.write(SetSelectModeTID);
          break;
        case "set_mode_user":
          port.write(SetSelectModeUSER);
          break;
        case "get_selected_param":
          port.write(GetSelectParams);
          break;
        default:
          console.log(`unknown action: ${action}`);
      }
    })
    .catch((error) => {
      console.log("inquire error", error);
    })
    .finally(() => {
      commandPrompt();
    });
}

//command = 49
//PL = 0x0000
//AP (access password) = 0x00000000
//Memback = 0x01 (EPC)
//SA (source address) = 0x0000
//DL (data length in uint16) = 0x0002
//data
// * CRC (current)
// * PC (current)
// * EPC
//checksum
//end
function writeEPC(PC, CRC, EPC) {
  const data = Buffer.concat([
    Buffer.from([0xbb, 0x00, COMMANDS.WRITE_LABEL_DATA_STORE]), // command
    Buffer.from([0x00, (4 + 1 + 2 + 2 + 2 + 2 + EPC.length)]), // PL
    Buffer.from([0x00, 0x00, 0x00, 0x00]), // AP
    Buffer.from([SEL_PARAMS.EPC]), // Memback
    Buffer.from([0x00, 0x00]), // SA

    // Data
    Buffer.from([0x00, (EPC.length + PC.length + CRC.length) / 2]), // DL
    CRC,
    PC,
    EPC,

    Buffer.from([0x00]), // checksum
    Buffer.from([0x7e]), // end
  ]);

  const finalData = fixChecksum(data);
  console.log(finalData.toString("hex"));
  return finalData;
}

function setSelectParams(EPC) {
  const data = Buffer.concat([
    Buffer.from([0xbb, 0x00, COMMANDS.SET_SELECT_PARAMS]), // command
    Buffer.from([0x00, 7 + EPC.length]), // PL
    Buffer.from([SEL_PARAMS.EPC]),
    Buffer.from([0x00, 0x00, 0x00, 0x20]), // ptr
    Buffer.from([0x60]), // maskLen // 0x60 is 96 bits, 6 words
    Buffer.from([0x00]), // trunc
    EPC,
    Buffer.from([0x00]), // checksum
    Buffer.from([0x7e]), // end
  ]);

  const finalData = fixChecksum(data);
  return finalData;
}

/**
 * @param {Buffer}
 * @return {bool}
 * @private
 */
function verifyChecksum(data) {
  const byteArray = new Uint8Array(data);
  const checkbyte = byteArray[byteArray.length - 2];
  const checksum = calculateChecksum(data);
  if (checkbyte !== checksum) {
    console.log({ checkbyte, checksum });
  }
  return checksum === checkbyte;
}

function calculateChecksum(data) {
  const byteArray = new Uint8Array(data);
  // Ignore start byte and end byte
  const content = byteArray.slice(1, byteArray.length - 2);
  const checksum = content.reduce((acc, val) => acc + val, 0) & 0xff;
  return checksum;
}

function fixChecksum(data) {
  const byteArray = new Uint8Array(data);
  const checksum = calculateChecksum(data);
  byteArray[byteArray.length - 2] = checksum;
  return Buffer.from(byteArray);
}

/**
 * \file
 * Functions and types for CRC checks.
 *
 * Generated on Thu Jan  9 16:14:48 2025
 * by pycrc v0.10.0, https://pycrc.org
 * using the configuration:
 *  - Width         = 16
 *  - Poly          = 0x1021
 *  - XorIn         = 0xffff
 *  - ReflectIn     = False
 *  - XorOut        = 0xffff
 *  - ReflectOut    = False
 *  - Algorithm     = table-driven
 */

/**
 * Static table used for the table_driven implementation.
 */
const crc_table = [
    0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
    0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
    0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
    0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
    0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
    0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
    0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
    0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
    0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
    0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
    0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
    0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
    0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
    0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
    0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
    0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
    0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
    0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
    0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
    0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
    0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
    0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
    0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
    0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
    0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
    0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
    0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
    0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
    0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
    0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
    0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
    0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
];

function crc_update(crc, data) {
    const d = new Uint8Array(data);
    let tbl_idx;

    for (let i = 0; i < d.length; i++) {
        tbl_idx = ((crc >> 8) ^ d[i]) & 0xff;
        crc = (crc_table[tbl_idx] ^ (crc << 8)) & 0xffff;
    }
    return (crc & 0xffff) ^ 0xffff;
}

function crc(data) {
    return crc_update(0xffff, data);
}
