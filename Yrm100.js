// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Yrm100 = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
var Yrm100 = (function() {
  Yrm100.Msgtype = Object.freeze({
    COMMAND: 0,
    RESPONSE: 1,
    NOTIFICATION: 2,

    0: "COMMAND",
    1: "RESPONSE",
    2: "NOTIFICATION",
  });

  function Yrm100(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Yrm100.prototype._read = function() {
    this.header = this._io.readU1();
    this.type = this._io.readU1();
    switch (this.type) {
    case Yrm100.Msgtype.COMMAND:
      this.payload = new Command(this._io, this, this._root);
      break;
    case Yrm100.Msgtype.RESPONSE:
      this.payload = new Response(this._io, this, this._root);
      break;
    case Yrm100.Msgtype.NOTIFICATION:
      this.payload = new Notification(this._io, this, this._root);
      break;
    }
    this.checksum = this._io.readU1();
    this.end = this._io.readU1();
  }

  var Command = Yrm100.Command = (function() {
    function Command(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Command.prototype._read = function() {
      this.command = this._io.readU1();
      this.lenParameter = this._io.readU2be();
      this.parameter = this._io.readBytes(this.lenParameter);
    }

    return Command;
  })();

  var Response = Yrm100.Response = (function() {
    function Response(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Response.prototype._read = function() {
      this.command = this._io.readU1();
      this.pl = this._io.readU2be();
      this.infoType = this._io.readU1();
      this.info = this._io.readBytes((this.pl - 1));
    }

    return Response;
  })();

  var Notification = Yrm100.Notification = (function() {
    function Notification(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Notification.prototype._read = function() {
      this.command = this._io.readU1();
      this.lenParameter = this._io.readU2be();
      this.rssi = this._io.readU1();
      this.pc = this._io.readU2be();
      this.epc = this._io.readBytes((this.lenParameter - 5));
      this.crc = this._io.readU2be();
    }

    return Notification;
  })();

  return Yrm100;
})();
return Yrm100;
}));
