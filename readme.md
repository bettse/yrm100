# YRM100

Node code for interacting with YRM100 (M5 UHF module) over USB-serial.

### Invocation

`SERIAL_PORT=/dev/cu.usbserial-0001 node index.js`

### Regenerate kaitai struct code

`kaitai-struct-compiler -t javascript yrm100.ksy`
